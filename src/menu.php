<?php
$menu=[
    [
        "titulo"=>"Home",
        "href"=>"index.php",
        "drop"=>false
    ],
    [
        "titulo"=>"Sedes",
        "href"=>"sedes.php",
        "drop"=>true,
        "dropMenu"=>[
                [
                    "titulo"=>"Madrid",
                    "href"=>"sedes.php?sede=MDR",
                ],
                [
                    "titulo"=>"Barcelona",
                    "href"=>"sedes.php?sede=BCN",
                ],
                [
                    "titulo"=>"Valencia",
                    "href"=>"sedes.php?sede=VLC",
                ]
            ]
    ],
    [
        "titulo"=>"Nueva Noticia",
        "href"=>"nuevanoticia.php",
        "drop"=>false
    ]
];
//var_dump($menu);
//print_r($menu);
/*
-- VERSION CON VARIABLES
$home="Home";
$noticia="Nueva Noticia";
*/
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
        <?php
            for ($i=0; $i < count($menu); $i++) { 
                if ($menu[$i]["drop"]) {?>
                    <li class='nav-item dropdown'>
                    <a class='nav-link dropdown-toggle' href='sedes.php' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                    Sedes
                    </a>
                    <div class='dropdown-menu' aria-labelledby='navbarDropdown'>
                    <?php
                    foreach ($menu[$i]["dropMenu"] as $key => $value) {
                    ?>
                    <a class='dropdown-item' href='sedes.php'><?=$value["titulo"]?></a>
                    <?php
                    }
                    echo "     </div>";
                    echo "     </li>";      
         
                }else{
                    echo "<li class='nav-item active'>";
                    echo "<a class='nav-link' href=".$menu[$i]["href"].">".$menu[$i]["titulo"]."<span class='sr-only'>(current)</span></a></li>";
                }
            }
        ?>


    </ul>
  </div>
</nav>