<?php
    $sede="todas";
    if(isset($_GET["sede"])){
        $sede=$_GET["sede"];
    }
?>

<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <?php
            include './src/menu.php';
        ?>
    <div class="container">
        <div class="row">
            <?php if(($sede=="MDR") || ($sede=="todas")){?>
            <div class="col-sm">
            <h2>Madrid</h2>
            </div>
            <?php }?>
            <?php if(($sede=="VLC") || ($sede=="todas")){?>
            <div class="col-sm">
            <h2>Valencia</h2>
            </div>
            <?php }?>
        </div>
        <div class="row">
            <?php if(($sede=="MDR") || ($sede=="todas")){?>
            <div class="col-sm">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3037.225662489615!2d-3.705861885171414!3d40.42600177936401!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd42286264887f17%3A0x78848aa7adfdb15f!2sCalle%20de%20San%20Andr%C3%A9s%2C%208%2C%2028004%20Madrid!5e0!3m2!1ses!2ses!4v1570017076508!5m2!1ses!2ses" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>
            <?php }?>
            <?php if(($sede=="VLC") || ($sede=="todas")){?>
            <div class="col-sm">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d49286.66045792326!2d-0.5303719995273692!3d39.45992349868825!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd60504003bffe4d%3A0x7aad53a204e4406e!2sAldaya%2C%2046960%2C%20Valencia!5e0!3m2!1ses!2ses!4v1570017411301!5m2!1ses!2ses" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe> 
            </div>
            <?php }?>
        </div>
    </div>
    <!-- CARGA DE LIBERRIAS JS-->  
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>